// Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

/*#######################################################################
                        GENERICS
########################################################################*/

/*
    Generic code enables you to write flexible, reusable functions and types that can work with any type, subject to requirements that you define.
 */

/************************************************************************
The Problem That Generics Solve
*************************************************************************/

//Standard Generic Problems
func swapTwoInts(inout a: Int, inout b: Int) { //function makes use of inout parameters
    let temporaryA = a
    a = b
    b = temporaryA
}

func swapTwoStrings(inout a: String, inout b: String) {
    let temporaryA = a
    a = b
    b = temporaryA
}

func swapTwoDoubles(inout a: Double, inout b: Double) {
    let temporaryA = a
    a = b
    b = temporaryA
}

//the bodies of the 3 function are identical. the only difference is the type of values they accept.

//It would be much more useful, and considerably more flexible, to write a single function that could swap two values of any type. This is the kind of problem that generic code can solve.

/************************************************************************
Generic Functions
*************************************************************************/

//e.g.
//Generic version of swapTwoInts function
func swapTwoValues<T>(inout a: T, inout b: T) { //T is the placeholder type instead of actual type
    let temporaryA = a
    a = b
    b = temporaryA
}

//e.g. T is inferred to be String and Int respectively
var someInt = 3
var anotherInt = 107
swapTwoValues(&someInt, &anotherInt)
//someInt is now 107, anotherInt is now 3
someInt
anotherInt

var someString = "hello"
var anotherString = "world"
swapTwoValues(&someString, &anotherString)
someString
anotherString

/************************************************************************
Type Parameters
*************************************************************************/

/*
    In the swapTwoValues example above, the placeholder type T is an example of a type parameter

    You can provide more than one type parameter by writing multiple type parameter names within the angle brackets, separated by commas.
 */

//e.g.
func sample<T, U>(a: T, b: U) {
    //sample code here
}

/************************************************************************
Naming Type Parameters
*************************************************************************/
//e.g
func sample2<KeyType, ValueType>(a: KeyType, b: ValueType) { //the type parameter are named keytype and value type to remind their purpose as you see them in your code.
    //sample code here
    
}

/************************************************************************
Generic Types
*************************************************************************/

/*
    Swift enables you to define your own generic types. These are custom classes, structures, and enumerations that can work with any type
 */

//e.g. Stack - push and pop item- push at first remove at first
//non generic example of stack
struct IntStack {
    var items = [Int]() //specific type
    mutating func push(item: Int) {
        items.append(item)
    }
    mutating func pop() -> Int {
        return items.removeLast()
    }
}

struct Stack2<T> {
    var items = [T]()
    mutating func push(item: T) { //T as the parameter
        items.append(item)
    }
    mutating func pop() -> T { //T as the return type
        return items.removeLast()
    }
}
//implementation
var stackOfStrings = Stack2<String>() //set the type to string
stackOfStrings.push("uno")
stackOfStrings.push("dos")
stackOfStrings.push("tres")
stackOfStrings.push("cuatro")
// the stack now contains 4 strings
stackOfStrings

let fromTheTop = stackOfStrings.pop() //remove the recently added "cuatro"

/************************************************************************
Extending a generic type
*************************************************************************/

/*
    When you extend a generic type, you do not provide a type parameter list as part of the extension’s definition. Instead, the type parameter list from the original type definition is available within the body of the extension, and the original type parameter names are used to refer to the type parameters from the original definition.
 */

//e.g
extension Stack2 {
    var topItem: T? { //T is already read within the Stack2 instantiation
        return items.isEmpty ? nil : items[items.count - 1]
    }
}

if let topItem = stackOfStrings.topItem {
    println("The top item on the stack is \(topItem).")
}

/************************************************************************
Type Constraints
*************************************************************************/

/*
    Type constraints specify that a type parameter must inherit from a specific class, or conform to a particular protocol or protocol composition.
 */

/************************************************************************
Type Constraint Syntax
*************************************************************************/

/*

 Syntax:

 func someFunction<T: SomeClass, U: SomeProtocol>(someT: T, someU: U) {
    //function body goes here
 }

*/

/************************************************************************
Type Constraints in Action
*************************************************************************/

func findStringIndex(array: [String], valueToFind: String) -> Int? {
    for (index, value) in enumerate(array) {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

//e.g. Implementation find string values in array
let strings = ["cat", "dog", "llama", "parakeet", "terrapin"]
if let foundIndex = findStringIndex(strings, "llama") {
    println("The index of llama is \(foundIndex)")
}
//This function is only useful for strings

//Generic version
func findIndex2<T>(array: [T], valueToFind: T) -> Int? {
    for (index, value) in enumerate(array) {
        //if value == valueToFind { //this == will not work because T is not something swift can guess for you.
            //return index
        //}
    }
    return nil
}

//Add the type Equatable to guarantee the T supports the equal operator
func findIndex<T: Equatable>(array: [T], valueToFind: T) -> Int? {// which means “any type T that conforms to the Equatable protocol.
    for (index, value) in enumerate(array) {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let doubleIndex = findIndex([3.14159, 0.1, 0.25], 9.3)
// doubleIndex is an optional Int with no value, because 9.3 is not in the array
let stringIndex = findIndex(["Mike", "Malcolm", "Andrea"], "Anrea")


/************************************************************************
Associated Types
*************************************************************************/

/*
    When defining a protocol, it is sometimes useful to declare one or more associated types as part of the protocol’s definition


 */

/************************************************************************
Associated Types in Action
*************************************************************************/

//e.g. Here’s an example of a protocol called Container, which declares an associated type called ItemType

protocol Container {
    typealias ItemType
    mutating func append(item: ItemType)
    var count: Int { get }
    subscript(i: Int) -> ItemType { get }
}

//Non generic version of IntStack
struct Instack: Container {
    // original IntStack implementation
    var items = [Int]()
    mutating func push(item: Int) {
        items.append(item)
    }
    mutating func pop() -> Int {
        return items.removeLast()
    }
    //conformance to the Container Protocol
    typealias ItemType = Int
    mutating func append(item: ItemType) {
        self.push(item)
    }
    var count: Int {
        return items.count
    }
    subscript(i: Int) ->Int {
        return items[i]
    }
}

//Generic Stack: conforms to protocol
// The T substitue the explicitly type Int
struct Stack<T>: Container { //must conforms to Container Protocol
    // original Stack<T> implementation
    var items = [T]()
    mutating func push(item: T) {
        items.append(item)
    }
    mutating func pop() -> T {
        return items.removeLast()
    }
    // conformance to the container Protocol
    mutating func append(item: T) {
        self.push(item)
    }
    var count: Int {
        return items.count
    }
    subscript(i: Int) -> T {
        return items[i]
    }
}

/************************************************************************
Extending an Existing Type to Specify an Associated Type
*************************************************************************/

/*
    Swift’s Array type already provides an append method, a count property, and a subscript with an Int index to retrieve its elements. These three capabilities match the requirements of the Container protocol. This means that you can extend Array to conform to the Container protocol simply by declaring that Array adopts the protoco.

    extension Array: Container {}
 */

/************************************************************************
Where clause
*************************************************************************/

/*
    The example below defines a generic function called allItemsMatch, which checks to see if two Container instances contain the same items in the same order
 */

func allItemsMatch<C1: Container, C2: Container where C1.ItemType == C2.ItemType, C2.ItemType: Equatable>(someContainer: C1, anotherContainer: C2) -> Bool {
    // check that both containers contain the same number of items
    if someContainer.count != anotherContainer.count {
        return false
    }
    
    // check each pair of items to see if they are equiavalent
    for i in 0..<someContainer.count {
        if someContainer[i] != anotherContainer[i] {
            return false
        }
    }
    
    //all items match, so return true
    return true
}

//Implementation
var stackOfStrings2 = Stack<String>()
stackOfStrings2.push("uno")
stackOfStrings2.push("dos")
stackOfStrings2.push("tres")

var arrayOfStrings = ["uno", "dos", "tres"]
if allItemsMatch(stackOfStrings2, arrayOfStrings) {
    println("All items match.")
} else {
    println("Not all items match.")
}
